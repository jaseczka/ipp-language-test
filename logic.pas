unit logic;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, Contnrs, FileUtil;

type
    TSentence = class
    protected
        function fIsGap(i: Integer): Boolean;
        function fIsEmptyGap(i: Integer): Boolean;
        function fIsCorrect(i: Integer): Boolean;
    private
        const
             { placeholder dla luki }
             GAP = '.....';
        var
           { lista słów w zdaniu, typ: TWord }
           Sentence: TObjectList;
           { lista słów w magazynie }
           Options: TStringList;
           { znaczniki dostępności słów w magazynie }
           EnabledOptions: Array of Boolean;

        { zwraca i-ty wyraz w zdaniu }
        function GetSentenceWord(i: Integer): String;
        { zwraca i-ty wyraz w magazynie }
        function GetOption(i: Integer): String;
    public
        const
            { lista znaków kończąca zdanie }
            { (ważne m.in. dla parsowania pliku z testami) }
            FinalMarks = '.?!';
            { lista znaków interpunkcyjnych nie kończących zdania }
            NonFinalMarks = ',;:';
        constructor Create(var sentenceStrings, optionsStrings: TStringList;
                    var gapIndices: TObjectList);
        destructor Destroy; override;
        { Ustawia opcję o indeksie source w luce, która jest słowem
          o indeksie target w zdaniu. Zwraca wartość logiczną sukcesu. }
        function SetWord(source, target: Integer): Boolean;
        { Zamienia słowa o zadanych indeksach w zdaniu, o ile są lukami.
          Zwraca wartość logiczną sukcesu. }
        function SwapWord(lhs, rhs: Integer): Boolean;
        { Zwraca liczbę poprawnych odpowiedzi. }
        function CountCorrect(): Integer;
        { Zwraca liczbę niepoprawnych odpowiedzi. }
        function CountWrong(): Integer;
        { Zwraca długość zdania. }
        function Length(): Integer;
        { Zwraca liczbę słów w magazynie. }
        function OptionsCount(): Integer;
        { Zwraca i-ty wyraz zdania }
        property SentenceWord[i: Integer]: String read GetSentenceWord; default;
        { Zwraca i-ty wyraz z magazynu }
        property Option[i: Integer]: String read GetOption;
        { Sprawdza czy i-te miejsce w zdaniu jest luką. }
        property IsGap[i: Integer]: Boolean read fIsGap;
        { Sprawdza czy i-te miejsce w zdaniu jest pustą luką. }
        property IsEmptyGap[i: Integer]: Boolean read fIsEmptyGap;
        { Sprawdza czy i-ty wyraz jest poprawnie uzupełnioną luką }
        property IsCorrect[i: Integer]: Boolean read fIsCorrect;
    end;

    TSentenceList = class
    protected
        { lista zdań typu TSentence }
        SentenceList: TObjectList;
        { indeks obecnie rozwiązywanego testu (zdania) }
        currentSentence: Integer;
        { liczba poprawnie uzupełnionych luk do tej pory }
        correct: Integer;
        { liczba niepoprawnie uzupełnionych luk do tej pory }
        wrong: Integer;
        function GetSentence(): TSentence;
        function fHasEnded(): Boolean;
    private
        { Dodaje zdanie do listy zdaN. }
        function Add(s: TSentence): Integer;
    public
        constructor Create;
        destructor Destroy; override;
        { Zwiększa indeks bieżącego zdania }
        procedure Next;
        { Zlicza poprawne i niepoprawne odpowiedzi }
        procedure Check;
        { Zwraca liczbę poprawnych odpowiedzi }
        property TotalCorrect: Integer read correct;
        { Zwraca liczbę niepoprawnych odpowiedzi }
        property TotalWrong: Integer read wrong;
        { Zwraca bieżące zdanie }
        property Current: TSentence read GetSentence;
        { Stwierdza czy osiągnięto koniec listy zdań }
        property HasEnded: Boolean read fHasEnded;
        { Zwraca liczbę zdań w zestawie testów }
        function Count(): Integer;
    end;

    TWord = class abstract
    protected
        pWord: String;
        pIsMutable: Boolean;
        procedure setWord(Word: String); virtual; abstract;
    public
        constructor Create(Word: String; IsMutable: Boolean);
        property Word: String read pWord write setWord;
        property IsMutable: Boolean read pIsMutable;

    end;

    TWordFixed = class(TWord)
    protected
        procedure setWord(aWord: String); override;
    public
        constructor Create(aWord: String);
    end;

    TWordMutable = class(TWord)
    protected
        answer: String;
        procedure setWord(aWord: String); override;
    public
        constructor Create(aWord, anAnswer: String);
        function isCorrect(): Boolean;
    end;

function SentencesFromFile(FileName: String): TSentenceList;

implementation

function TSentenceList.Count(): Integer;
begin
    Count := SentenceList.Count;
end;

constructor TSentence.Create(var sentenceStrings, optionsStrings: TStringList;
            var gapIndices: TObjectList);
var
  i, j: Integer;
  w: TWord;
begin
    Sentence := TObjectList.Create(True);
    Sentence.Capacity := sentenceStrings.Count;
    Options := TStringList.Create;
    Options.Capacity := gapIndices.Count + optionsStrings.Count;
    j := 0;
    for i := 0 to sentenceStrings.Count - 1 do
    begin
        if (j < gapIndices.Count) and (i = Integer(gapIndices[j])) then
        begin
            w := TWordMutable.Create(GAP, sentenceStrings[i]);
            Sentence.Add(w);
            Options.Add(sentenceStrings[i]);
            Inc(j);
        end
        else
        begin
            w := TWordFixed.Create(sentenceStrings[i]);
            Sentence.Add(w);
        end;
    end;
    Options.AddStrings(optionsStrings);

    for i := Options.Count - 1 downto 1 do
        Options.Exchange(i, Random(i + 1));

    SetLength(EnabledOptions, Options.Count);
    for i := Low(EnabledOptions) to High(EnabledOptions) do
        EnabledOptions[i] := True;
end;

destructor TSentence.Destroy();
begin
    Sentence.Free;
    Options.Free;
    SetLength(EnabledOptions, 0);
    inherited Destroy;
end;

function TSentence.fIsGap(i: Integer): Boolean;
begin
    fIsGap := TWord(Sentence[i]).IsMutable;
end;

function TSentence.fIsEmptyGap(i: Integer): Boolean;
begin
    fIsEmptyGap := ((TWord(Sentence[i]).IsMutable)
                and (TWord(Sentence[i]).Word = GAP));
end;

function TSentence.fIsCorrect(i: Integer): Boolean;
begin
    assert(IsGap[i]);
    fIsCorrect := TWordMutable(Sentence[i]).IsCorrect();
end;

function TSentence.GetSentenceWord(i: Integer): String;
begin
    GetSentenceWord := TWord(Sentence[i]).Word;
end;

function TSentence.GetOption(i: Integer): String;
begin

    GetOption := Options[i];
end;

function TSentence.setWord(source, target: Integer): Boolean;
begin
    if (source >= Options.Count) or (target >= Sentence.Count) then
    begin
      setWord := False;
      exit;
    end;
    if (not IsGap[target]) or (not IsEmptyGap[target]) then
    begin
      setWord := False;
      exit;
    end;
    if not EnabledOptions[source] then
    begin
      setWord := False;
      exit;
    end;
    TWordMutable(Sentence[target]).Word := Options[source];
    EnabledOptions[source] := False;
    setWord := True;
end;

function TSentence.swapWord(lhs, rhs: Integer): Boolean;
var
  tmp: String;
begin
    if (lhs >= Sentence.Count) or (rhs >= Sentence.Count) then
    begin
      swapWord := False;
      exit;
    end;
    if (not IsGap[lhs]) or (not IsGap[rhs]) then
    begin
      swapWord := False;
      exit;
    end;
    tmp := TWord(Sentence[lhs]).Word;
    TWordMutable(Sentence[lhs]).Word := TWordMutable(Sentence[rhs]).Word;
    TWordMutable(Sentence[rhs]).Word := tmp;
    swapWord := True;
end;

function TSentence.CountCorrect(): Integer;
var
  i, count: Integer;
begin
    count := 0;
    for i := 0 to Sentence.Count - 1 do
    begin
        if TWord(Sentence[i]).IsMutable then
        begin
          if TWordMutable(Sentence[i]).isCorrect() then Inc(count);
        end;
    end;
    CountCorrect := count;
end;

function TSentence.CountWrong(): Integer;
var
  i, count: Integer;
begin
    count := 0;
    for i := 0 to Sentence.Count - 1 do
    begin
        if TWord(Sentence[i]).IsMutable then
        begin
          if not TWordMutable(Sentence[i]).isCorrect() then Inc(count);
        end;
    end;
    CountWrong := count;
end;

function TSentence.Length(): Integer;
begin
    Length := Sentence.Count;
end;

function TSentence.OptionsCount(): Integer;
begin
    OptionsCount := Options.Count;
end;

constructor TWord.Create(Word: String; IsMutable: Boolean);
begin
    pWord := Word;
    pIsMutable := IsMutable;
end;

constructor TWordFixed.Create(aWord: String);
begin
    inherited Create(aWord, False);
end;

constructor TWordMutable.Create(aWord, anAnswer: String);
begin
    inherited Create(aWord, True);
    answer := anAnswer;
end;

procedure TWordFixed.setWord(aWord: String);
begin
    { nic nie robi }
end;

procedure TWordMutable.setWord(aWord: String);
begin
    pWord := aWord;
end;

function TWordMutable.isCorrect(): Boolean;
begin
    isCorrect := (pWord = answer);
end;

function SentenceFromString(var line: TStringList): TSentence;
var
    i, j: Integer;
    sentence, options: TStringList;
    gaps: TObjectList;
    isSentence: Boolean;
    word: String;
begin
    isSentence := True;
    sentence := TStringList.Create;
    options := TStringList.Create;
    gaps := TObjectList.Create;
    j := 0;
    for i := 0 to line.Count - 1 do
    begin
        word := line[i];
        Assert(Length(word) > 0);
        if isSentence then
        begin
            if word[1] = '|' then
            begin
                gaps.Add(TObject(j));
                word := Copy(word, 2, Length(word) - 1);
            end;
            if Pos(word[Length(word)], TSentence.FinalMarks) <> 0
                then isSentence := False;
            if (Pos(word[Length(word)], TSentence.FinalMarks) <> 0)
                    or (Pos(word[Length(word)], TSentence.NonFinalMarks) <> 0) then
                begin
                    sentence.Add(Copy(word, 1, Length(word) - 1));
                    sentence.Add(word[Length(word)]);
                    Inc(j);
                end
                else sentence.Add(word);
            Inc(j);
        end
        else { not isSentence }
        begin
            options.Add(word);
        end;
    end;
    SentenceFromString := TSentence.Create(sentence, options, gaps);
end;

function SentencesFromFile(FileName: String): TSentenceList;
var
  f, line: TStringList;
  slist: TSentenceList;
  i: Integer;
  s: TSentence;
begin
    f := TStringList.Create;
    f.LoadFromFile(Utf8ToAnsi(FileName));
    slist := TSentenceList.Create;
    for i := 0 to f.Count - 1 do
    begin
        line := TStringList.Create;
        line.Delimiter := ' ';
        line.DelimitedText := Utf8Encode(f[i]);
        s := SentenceFromString(line);
        slist.Add(s);
        line.Free;
    end;
    f.Free;
    SentencesFromFile := slist;
end;

constructor TSentenceList.Create;
begin
    SentenceList := TObjectList.Create(True);
    currentSentence := 0;
    correct := 0;
    wrong   := 0;
end;

destructor TSentenceList.Destroy;
begin
    SentenceList.Free;
    inherited Destroy;
end;

function TSentenceList.Add(s: TSentence): Integer;
begin
    Add := SentenceList.Add(s);
end;

function TSentenceList.GetSentence(): TSentence;
begin
    if not HasEnded
       then GetSentence := TSentence(SentenceList[currentSentence])
       else GetSentence := nil;
end;

function TSentenceList.fHasEnded(): Boolean;
begin
    fHasEnded := (currentSentence >= SentenceList.Count);
end;

procedure TSentenceList.Next;
begin
    if not HasEnded then Inc(currentSentence);
end;

procedure TSentenceList.Check;
var
    i: Integer;
begin
    for i := 0 to Current.Length() - 1 do
    begin
        if TWord(Current.Sentence[i]).IsMutable then
        begin
          if TWordMutable(Current.Sentence[i]).isCorrect()
             then Inc(correct)
             else Inc(wrong);
        end;
    end;
end;

initialization

Randomize;

end.

