unit gui;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics,
    Dialogs, ExtCtrls, StdCtrls, Grids, Contnrs, logic;

type

    { TForm1 }

    TForm1 = class(TForm)
        About: TButton;
        LoadTestsButton: TButton;
        CloseButton: TButton;
        AllAnswersLabel: TLabel;
        AllAnswersCounterLabel: TLabel;
        OpenDialog: TOpenDialog;
        WordsScrollBox: TScrollBox;
        SentenceScrollBox: TScrollBox;
        WrongAnswersCounterLabel: TLabel;
        WrongAnswersLabel: TLabel;
        CorrectAnswersCounterLabel: TLabel;
        CorrectAnswersLabel: TLabel;
        StatusBox: TGroupBox;
        NextButton: TButton;
        CheckButton: TButton;
        MenuPanel: TPanel;
        ContentPanel: TPanel;
        SentenceWordsSplitter: TSplitter;
        WordsPanel: TPanel;
        SentencePanel: TPanel;

        procedure AboutClick(Sender: TObject);
        procedure CheckButtonClick(Sender: TObject);
        procedure CloseButtonClick(Sender: TObject);
        procedure FormCloseQuery(Sender: TObject; var CanClose: boolean);
        procedure FormCreate(Sender: TObject);
        procedure FormDestroy(Sender: TObject);
        procedure LoadTestsButtonClick(Sender: TObject);
        procedure NextButtonClick(Sender: TObject);
        procedure SentenceLabelClick(Sender: TObject);
        procedure SentenceLabelDragOver(Sender, Source: TObject; X, Y: Integer;
                    State: TDragState; var Accept: Boolean);
        procedure SentenceLabelDragDrop(Sender, Source: TObject; X, Y: Integer);
        procedure SentencePanelResize(Sender: TObject);
        procedure WordsPanelResize(Sender: TObject);
    protected
        { lista zdań typu TSentence }
        Sentences: TSentenceList;
        { tworzy słowa w zdaniu i magazynie }
        procedure CreateLabels;
        { czyści panele i zeruje liczniki }
        procedure ClearForm;
        { pozycjonuje słowa z podanej listy w zadanym panelu }
        procedure PaintLabels(Labels: TObjectList; Panel: TPanel);
        { pozycjonuje słowa w zdaniu i magazynie }
        procedure PaintPanelsContents;
        { wyświetla napis końcowy }
        procedure ShowEnd;
        { blokuje przeciąganie słów w zdaniu i magazynie }
        procedure BlockDrag;
    private
        SentenceLabels: TObjectList;
        WordsLabels: TObjectList;

    public
        { public declarations }
    end;

var
    Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
    SentenceLabels := TObjectList.Create(True);
    WordsLabels := TObjectList.Create(True);
    NextButton.Enabled := False;
    CheckButton.Enabled := False;
end;

procedure TForm1.FormDestroy(Sender: TObject);
begin
    Sentences.Free;
    SentenceLabels.Free;
    WordsLabels.Free;
end;

procedure TForm1.CloseButtonClick(Sender: TObject);
begin
    Close;
end;

procedure TForm1.FormCloseQuery(Sender: TObject; var CanClose: boolean);
begin
    if MessageDlg('Na pewno chcesz zakończyć?', mtConfirmation,
       [mbOk, mbCancel], 0) = mrOk
            then CanClose := True
            else CanClose := False;
end;

procedure TForm1.LoadTestsButtonClick(Sender: TObject);
begin
    if OpenDialog.Execute then
    begin
        NextButton.Enabled:=False;
        CheckButton.Enabled:=False;
        Sentences.Free;
        Sentences := SentencesFromFile(OpenDialog.FileName);
        ClearForm;
        CreateLabels;
        PaintPanelsContents;
        if Sentences.HasEnded then begin
            CheckButton.Enabled := False;
        end
        else
        begin
            CheckButton.Enabled := True;
        end;
    end;
end;

procedure TForm1.CheckButtonClick(Sender: TObject);
    procedure PaintAnswers;
    var
        i: Integer;
    begin
        for i := 0 to Sentences.Current.Length() - 1 do
            if Sentences.Current.IsGap[i]
               then if Sentences.Current.IsCorrect[i]
                   then TLabel(SentenceLabels[i]).Font.Color := clGreen
                   else TLabel(SentenceLabels[i]).Font.Color := clRed;
    end;

begin
    BlockDrag;
    Sentences.Check;

    CorrectAnswersCounterLabel.Caption := IntToStr(Sentences.TotalCorrect);
    WrongAnswersCounterLabel.Caption   := IntToStr(Sentences.TotalWrong);
    AllAnswersCounterLabel.Caption     :=
            IntToStr(Sentences.TotalCorrect + Sentences.TotalWrong);
    CheckButton.Enabled := False;
    NextButton.Enabled  := True;
    PaintAnswers;
end;

procedure TForm1.AboutClick(Sender: TObject);
begin
    ShowMessage(
        'Przydatne informacje: '
        + sLineBreak + sLineBreak +
        ' - Pola należy uzupełniać poprzez przeciąganie wyrazów myszą.'
        + sLineBreak +
        ' - Przeciągnięte wyrazy można przesuwać w ramach zdania.'
        + sLineBreak +
        ' - Kolejność słów w magazynie jest losowana przy każdym ładowaniu testów.'
        + sLineBreak +
        ' - Aby przejść do następnego zdania, należy je najpierw sprawdzić.'
        + sLineBreak + sLineBreak +
        'Autor:' + sLineBreak + 'Magdalena Teperowska'
        );
end;

procedure TForm1.NextButtonClick(Sender: TObject);
begin
    Sentences.Next;

    NextButton.Enabled := False;
    if Sentences.HasEnded then
    begin
        CheckButton.Enabled := False;
    end
    else
    begin
        CheckButton.Enabled := True;
    end;
    SentenceLabels.Clear;
    WordsLabels.Clear;
    CreateLabels;
    PaintPanelsContents;
end;

procedure TForm1.SentenceLabelClick(Sender: TObject);
begin
    Tlabel(Sender).Color:=clBlue;
end;

procedure TForm1.BlockDrag;
var
    i: Integer;
begin
    for i := 0 to SentenceLabels.Count - 1 do
        TLabel(SentenceLabels[i]).DragMode := dmManual;
    for i := 0 to WordsLabels.Count - 1 do
        TLabel(WordsLabels[i]).DragMode := dmManual;
end;

procedure TForm1.SentenceLabelDragOver(Sender, Source: TObject; X, Y: Integer;
                    State: TDragState; var Accept: Boolean);
var
    DstL: TLabel;
    dstI: Integer;
begin
    if TLabel(Source).Parent = WordsPanel then
    begin
        DstL := TLabel(Sender);
        dstI := SentenceLabels.IndexOf(DstL);
        if Sentences.Current.IsEmptyGap[dstI]
           then Accept := True
           else Accept := False;
    end
    else Accept := True;
end;

procedure TForm1.SentenceLabelDragDrop(Sender, Source: TObject; X, Y: Integer);
var
    SrcL, DstL: TLabel;
    srcI, dstI: Integer;
begin
    if Sender = Source then exit;
    SrcL := TLabel(Source);
    DstL := TLabel(Sender);
    dstI := SentenceLabels.IndexOf(DstL);
    if SrcL.Parent = WordsPanel then
    { wstawienie słowa }
    begin
        srcI := WordsLabels.IndexOf(srcL);
        if Sentences.Current.SetWord(srcI, dstI) then
        begin
            DstL.Caption  := Sentences.Current[dstI];
            DstL.DragMode := dmAutomatic;
            SrcL.Hide;
            PaintLabels(SentenceLabels, SentencePanel);
        end;
    end
    else
    if SrcL.Parent = SentencePanel then
    { zamiana słów }
    begin
        srcI := SentenceLabels.IndexOf(srcL);
        Assert(Sentences.Current.IsEmptyGap[srcI] = False);
        if Sentences.Current.SwapWord(srcI, dstI) then
        begin
            DstL.Caption := Sentences.Current[dstI];
            SrcL.Caption := Sentences.Current[srcI];
            { teraz w src może być puste miejsce }
            if Sentences.Current.IsEmptyGap[srcI] then
            begin
                SrcL.DragMode := dmManual;
                DstL.DragMode := dmAutomatic;
            end;
            PaintLabels(SentenceLabels, SentencePanel);
        end;
    end;
end;

procedure TForm1.SentencePanelResize(Sender: TObject);
begin
    PaintLabels(SentenceLabels, SentencePanel);
end;

procedure TForm1.WordsPanelResize(Sender: TObject);
begin
    PaintLabels(WordsLabels, WordsPanel);
end;

procedure TForm1.ClearForm;
begin
    SentenceLabels.Clear;
    WordsLabels.Clear;
    CorrectAnswersCounterLabel.Caption := '0';
    WrongAnswersCounterLabel.Caption   := '0';
    AllAnswersCounterLabel.Caption     := '0';
end;

procedure TForm1.CreateLabels;
var
    i: Integer;
    s: TSentence;
    l: TLabel;
begin
    Assert(Sentences <> nil);
    if Sentences.HasEnded then
    begin
        ShowEnd;
        exit;
    end;

    s := Sentences.Current;
    SentenceLabels.Capacity := s.Length();
    WordsLabels.Capacity := s.OptionsCount();

    for i := 0 to s.Length() - 1 do
    begin
        l := TLabel.Create(Self);
        l.Caption := s[i];
        l.Parent  := SentencePanel;
        if s.IsGap[i] then
        begin
            l.Font.Style := [fsBold];
            l.OnDragOver := @SentenceLabelDragOver;
            l.OnDragDrop := @SentenceLabelDragDrop;
        end;
        SentenceLabels.Add(l);
    end;

    for i := 0 to s.OptionsCount() - 1 do
    begin
        WordsLabels.Add(TLabel.Create(Self));
        TLabel(WordsLabels[i]).Caption  := s.Option[i];
        TLabel(WordsLabels[i]).DragMode := dmAutomatic;
        TLabel(WordsLabels[i]).Parent   := WordsPanel;
    end;

end;

procedure TForm1.PaintLabels(Labels: TObjectList; Panel: TPanel);
var
    i, leftPos, topPos, space, innerWidth, margin: Integer;
    l: TLabel;
begin
    topPos := 0;
    leftPos := 0;
    space := 3;
    margin := 5;
    innerWidth := Panel.ClientWidth - 2 * Panel.BevelWidth;
    for i := 0 to Labels.Count - 1 do
    begin
        l := TLabel(Labels[i]);
        l.Align  := alNone;
        { l nie jest znakiem interpunkcyjnym }
        if (Pos(l.Caption, TSentence.FinalMarks) = 0)
                and (Pos(l.Caption, TSentence.NonFinalMarks) = 0) then
        begin
            if leftPos + l.Canvas.TextWidth(l.Caption) >= innerWidth - margin then
            begin
                leftPos := space;
                TopPos := TopPos + l.Canvas.TextHeight(l.Caption);
            end
            else
            begin
                leftPos := leftPos + space;
            end;
        end;
        l.Left   := leftPos;
        l.Top    := topPos;
        leftPos := leftPos + l.Canvas.TextWidth(l.Caption);
    end;
end;

procedure TForm1.PaintPanelsContents;
begin
    PaintLabels(SentenceLabels, SentencePanel);
    PaintLabels(WordsLabels, WordsPanel);
end;

procedure TForm1.ShowEnd;
begin
    SentenceLabels.Add(TLabel.Create(Self));
    TLabel(SentenceLabels.Last).Caption   := 'Koniec';
    TLabel(SentenceLabels.Last).Align     := alClient;
    TLabel(SentenceLabels.Last).Alignment := taCenter;
    TLabel(SentenceLabels.Last).Parent    := SentencePanel;
    TLabel(SentenceLabels.Last).Show;
end;

end.

